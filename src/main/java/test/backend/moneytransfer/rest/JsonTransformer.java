package test.backend.moneytransfer.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.Validate;
import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String render(Object object) throws Exception {
        return object != null ? objectMapper.writeValueAsString(object) : null;
    }
}
