package test.backend.moneytransfer.rest;

import spark.Service;

public interface RestController {

    void configureEndpoints(Service spark, String path);
}
