package test.backend.moneytransfer.exception;

import java.sql.SQLException;

public class TransactionException extends SQLException {
    private String message;

    public TransactionException(String message) {
        super(message);
    }
}
