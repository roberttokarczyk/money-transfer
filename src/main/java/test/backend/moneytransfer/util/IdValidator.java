package test.backend.moneytransfer.util;

public class IdValidator {

    public boolean isValid(Long id) {
        return id != null && id > 0;
    }
}
