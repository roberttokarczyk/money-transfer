package test.backend.moneytransfer.user.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.user.domain.User;

import java.sql.SQLException;

public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UsersDao {

    public UserDaoImpl() throws SQLException {
        super(DataSourceConfiguration.getConnectionSource(), User.class);
    }
}
