package test.backend.moneytransfer.user.dao;

import com.j256.ormlite.dao.Dao;
import test.backend.moneytransfer.user.domain.User;

public interface UsersDao extends Dao<User, Long> {
}
