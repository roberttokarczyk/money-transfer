package test.backend.moneytransfer.user.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import test.backend.moneytransfer.domain.BaseEntity;

@DatabaseTable(tableName = "users")
public class User extends BaseEntity {

    @DatabaseField(generatedId = true, canBeNull = false)
    private Long id;

    @DatabaseField(columnName = "firstName", canBeNull = false)
    private String firstName;

    @DatabaseField(columnName = "lastName", canBeNull = false)
    private String lastName;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
