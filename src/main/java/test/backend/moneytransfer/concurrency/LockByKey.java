package test.backend.moneytransfer.concurrency;

import java.util.concurrent.locks.ReadWriteLock;

public interface LockByKey<T> {

    ReadWriteLock getLock(T key);
}
