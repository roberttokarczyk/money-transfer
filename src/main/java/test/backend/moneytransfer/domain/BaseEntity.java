package test.backend.moneytransfer.domain;

import com.j256.ormlite.field.DatabaseField;

import java.util.Objects;

public abstract class BaseEntity {
    public abstract Long getId();

    public abstract void setId(Long id);

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        BaseEntity that = (BaseEntity) object;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
