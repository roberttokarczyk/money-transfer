package test.backend.moneytransfer.account.domain;

enum Currency {
    PLN,
    EUR,
    USD
}
