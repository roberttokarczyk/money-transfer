package test.backend.moneytransfer.account.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.account.concurrency.AccountLockByKey;
import test.backend.moneytransfer.concurrency.LockByKey;
import test.backend.moneytransfer.domain.BaseEntity;
import test.backend.moneytransfer.transaction.domain.Transaction;
import test.backend.moneytransfer.user.domain.User;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.concurrent.locks.Lock;

@DatabaseTable(tableName = "account")
public class Account extends BaseEntity {

    @DatabaseField(generatedId = true, canBeNull = false)
    private Long id;

    @DatabaseField(columnName = "number", canBeNull = false)
    private String number;

    @DatabaseField(columnName = "balance", canBeNull = false)
    private BigDecimal balance;

    @DatabaseField(columnName = "ownerId", foreign = true, foreignAutoRefresh = true)
    private User owner;

    @DatabaseField(columnName = "currency", canBeNull = false, dataType = DataType.ENUM_STRING)
    private Currency currency;

    @DatabaseField(columnName = "inactive", canBeNull = false)
    private boolean inactive;

    @ForeignCollectionField(foreignFieldName = "sourceAccount")
    private Collection<Transaction> outgoingTransaction;

    @ForeignCollectionField(foreignFieldName = "targetAccount")
    private Collection<Transaction> incomingTransactions;

    private final LockByKey<Long> lockByKey = new AccountLockByKey();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getBalance() {
        Lock readLock = this.lockByKey.getLock(this.id).readLock();
        readLock.lock();
        try {
            return balance;
        } finally {
            readLock.unlock();
        }
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public boolean isInactive() {
        return inactive;
    }

    public Collection<Transaction> getOutgoingTransaction() {
        return outgoingTransaction;
    }

    public void setOutgoingTransaction(Collection<Transaction> outgoingTransaction) {
        this.outgoingTransaction = outgoingTransaction;
    }

    public Collection<Transaction> getIncomingTransactions() {
        return incomingTransactions;
    }

    public void setIncomingTransactions(Collection<Transaction> incomingTransactions) {
        this.incomingTransactions = incomingTransactions;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public boolean debit(BigDecimal amount) {
        Validate.isTrue(amount.signum() > 0);
        BigDecimal newBalance = this.balance.subtract(amount);
        if (newBalance.signum() >= 0) {
            this.balance = newBalance;
            return true;
        }
        return false;
    }

    public boolean credit(BigDecimal amount) {
        Validate.isTrue(amount.signum() > 0);

        this.balance = this.balance.add(amount);
        return true;
    }
}
