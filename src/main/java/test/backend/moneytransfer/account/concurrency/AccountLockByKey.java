package test.backend.moneytransfer.account.concurrency;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.concurrency.LockByKey;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AccountLockByKey implements LockByKey<Long> {

    private final static ConcurrentHashMap<Long, ReadWriteLock> LOCKS_MAP = new ConcurrentHashMap<>();

    @Override
    public ReadWriteLock getLock(Long accountId) {
        Validate.notNull(accountId);

        return LOCKS_MAP.computeIfAbsent(accountId, key -> new ReentrantReadWriteLock());
    }
}
