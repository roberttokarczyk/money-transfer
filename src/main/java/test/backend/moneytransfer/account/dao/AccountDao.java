package test.backend.moneytransfer.account.dao;

import com.j256.ormlite.dao.Dao;
import test.backend.moneytransfer.account.domain.Account;

public interface AccountDao extends Dao<Account, Long> {
}
