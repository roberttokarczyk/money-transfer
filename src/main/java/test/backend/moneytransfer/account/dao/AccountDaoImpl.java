package test.backend.moneytransfer.account.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.account.domain.Account;

import java.sql.SQLException;

public class AccountDaoImpl extends BaseDaoImpl<Account, Long> implements AccountDao  {

    public AccountDaoImpl() throws SQLException {
        super(DataSourceConfiguration.getConnectionSource(), Account.class);
    }
}
