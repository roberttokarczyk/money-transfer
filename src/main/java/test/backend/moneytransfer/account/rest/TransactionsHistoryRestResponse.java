package test.backend.moneytransfer.account.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class TransactionsHistoryRestResponse {
    private final List<TransactionDetails> transactionsDetails;

    TransactionsHistoryRestResponse(List<TransactionDetails> transactionDetails) {
        this.transactionsDetails = transactionDetails;
    }

    public List<TransactionDetails> getTransactionsDetails() {
        return transactionsDetails;
    }

    public static class TransactionDetails {
        private final Participant sender;
        private final AccountDetails sourceAccount;
        private final Participant receiver;
        private final AccountDetails targetAccount;
        private final BigDecimal amount;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private final Date transactionDate;
        private final String transactionStatus;
        private final String transactionType;

        TransactionDetails(Participant sender, AccountDetails sourceAccount, Participant receiver, AccountDetails targetAccount, BigDecimal amount,
                                   Date transactionDate, String transactionStatus, String transactionType) {
            this.sender = sender;
            this.sourceAccount = sourceAccount;
            this.receiver = receiver;
            this.targetAccount = targetAccount;
            this.amount = amount;
            this.transactionDate = transactionDate;
            this.transactionStatus = transactionStatus;
            this.transactionType = transactionType;
        }

        public Participant getSender() {
            return sender;
        }

        public AccountDetails getSourceAccount() {
            return sourceAccount;
        }

        public Participant getReceiver() {
            return receiver;
        }

        public AccountDetails getTargetAccount() {
            return targetAccount;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public Date getTransactionDate() {
            return transactionDate;
        }

        public String getTransactionStatus() {
            return transactionStatus;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public static class Participant {
            private final String firstName;
            private final String lastName;

            Participant(String firstName, String lastName) {
                this.firstName = firstName;
                this.lastName = lastName;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }
        }

        public static class AccountDetails {
            private final String number;

            AccountDetails(String number) {
                this.number = number;
            }

            public String getNumber() {
                return number;
            }
        }
    }
}
