package test.backend.moneytransfer.account.rest;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.account.rest.TransactionsHistoryRestResponse.TransactionDetails;
import test.backend.moneytransfer.account.service.TransactionsHistory;

import java.util.List;
import java.util.stream.Collectors;

class TransactionsHistoryMapper {
    TransactionsHistoryRestResponse toRestResponse(TransactionsHistory transactionsHistory) {
        Validate.notNull(transactionsHistory);

        List<TransactionDetails> transactionsDetails = transactionsHistory.getTransactionsDetails()
                .stream()
                .map(this::mapTransactionDetails)
                .collect(Collectors.toList());

        return new TransactionsHistoryRestResponse(transactionsDetails);
    }

    private TransactionDetails mapTransactionDetails(TransactionsHistory.TransactionDetails transactionDetails) {
        TransactionDetails.Participant sender = mapParticipant(transactionDetails.getSender());
        TransactionDetails.AccountDetails sourceAccountDetails = mapAccountDetails(transactionDetails.getSourceAccount());
        TransactionDetails.Participant receiver = mapParticipant(transactionDetails.getReceiver());
        TransactionDetails.AccountDetails targetAccountDetails = mapAccountDetails(transactionDetails.getTargetAccount());
        return new TransactionDetails(sender, sourceAccountDetails, receiver, targetAccountDetails, transactionDetails.getAmount(),
                                      transactionDetails.getTransactionDate(), transactionDetails.getTransactionStatus().name(),
                                      transactionDetails.getTransactionType().name());
    }

    private TransactionDetails.Participant mapParticipant(TransactionsHistory.TransactionDetails.Participant participant) {
        return new TransactionDetails.Participant(participant.getFirstName(), participant.getLastName());
    }

    private TransactionDetails.AccountDetails mapAccountDetails(TransactionsHistory.TransactionDetails.AccountDetails accountDetails) {
        return new TransactionDetails.AccountDetails(accountDetails.getNumber());
    }
}
