package test.backend.moneytransfer.account.rest;

import spark.ResponseTransformer;
import spark.Service;
import test.backend.moneytransfer.account.service.AccountId;
import test.backend.moneytransfer.account.service.AccountService;
import test.backend.moneytransfer.account.service.TransactionsHistory;
import test.backend.moneytransfer.rest.JsonTransformer;
import test.backend.moneytransfer.rest.RestController;
import test.backend.moneytransfer.util.IdValidator;

import javax.servlet.http.HttpServletResponse;

public class AccountController implements RestController {
    private final ResponseTransformer responseTransformer = new JsonTransformer();
    private final TransactionsHistoryMapper transactionsHistoryMapper = new TransactionsHistoryMapper();
    private final AccountService accountService = new AccountService();
    private final IdValidator idValidator = new IdValidator();

    @Override
    public void configureEndpoints(Service spark, String path) {
        configureGetAccountTransactionsEndpoint(spark, path);
    }

    private void configureGetAccountTransactionsEndpoint(Service spark, String path) {
        spark.get(path + "/accounts/:accountId/transactions", "application/json", (request, response) -> {
            String accountIdValue = request.params("accountId");

            if (!accountIdIsValid(accountIdValue)) {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return "";
            }

            AccountId accountId = new AccountId(Long.valueOf(accountIdValue));
            TransactionsHistory accountHistory = accountService.getAccountHistory(accountId);
            if (accountHistory == null) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
                return "";
            } else {
                response.status(HttpServletResponse.SC_OK);
                return transactionsHistoryMapper.toRestResponse(accountHistory);
            }
        }, responseTransformer);
    }

    private boolean accountIdIsValid(String accountIdValue) {
        if (accountIdValue == null || accountIdValue.isEmpty()) {
            return false;
        }

        try {
            Long accountId = Long.valueOf(accountIdValue);
            return idValidator.isValid(accountId);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
