package test.backend.moneytransfer.account.service;

import org.apache.commons.lang3.Validate;
import spark.utils.CollectionUtils;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.transaction.domain.Transaction;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;
import test.backend.moneytransfer.user.domain.User;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransactionsHistory {
    private final List<TransactionDetails> transactionsDetails;

    private TransactionsHistory(List<TransactionDetails> transactionDetails) {
        this.transactionsDetails = transactionDetails;
    }

    public List<TransactionDetails> getTransactionsDetails() {
        return transactionsDetails;
    }

    static Builder builder() {
        return new Builder();
    }

    public static class TransactionDetails {
        private final Participant sender;
        private final AccountDetails sourceAccount;
        private final Participant receiver;
        private final AccountDetails targetAccount;
        private final BigDecimal amount;
        private final Date transactionDate;
        private final TransactionStatus transactionStatus;
        private final TransactionType transactionType;

        private TransactionDetails(Participant sender, AccountDetails sourceAccount, Participant receiver, AccountDetails targetAccount, BigDecimal amount,
                                   Date transactionDate, TransactionStatus transactionStatus, TransactionType transactionType) {
            this.sender = sender;
            this.sourceAccount = sourceAccount;
            this.receiver = receiver;
            this.targetAccount = targetAccount;
            this.amount = amount;
            this.transactionDate = transactionDate;
            this.transactionStatus = transactionStatus;
            this.transactionType = transactionType;
        }

        public Participant getSender() {
            return sender;
        }

        public AccountDetails getSourceAccount() {
            return sourceAccount;
        }

        public Participant getReceiver() {
            return receiver;
        }

        public AccountDetails getTargetAccount() {
            return targetAccount;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public Date getTransactionDate() {
            return transactionDate;
        }

        public TransactionStatus getTransactionStatus() {
            return transactionStatus;
        }

        public TransactionType getTransactionType() {
            return transactionType;
        }

        static Builder builder() {
            return new Builder();
        }

        public static class Participant {
            private final String firstName;
            private final String lastName;

            private Participant(String firstName, String lastName) {
                this.firstName = firstName;
                this.lastName = lastName;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }
        }

        public static class AccountDetails {
            private final String number;

            private AccountDetails(String number) {
                this.number = number;
            }

            public String getNumber() {
                return number;
            }
        }

        public enum TransactionType {
            INCOMING,
            OUTGOING
        }

        static class Builder {
            private Account sourceAccount;
            private Account targetAccount;
            private BigDecimal amount;
            private Date transactionDateTime;
            private TransactionStatus transactionStatus;
            private AccountId transactionDetailsForAccount;

            private Builder() {
            }

            Builder withSourceAccount(Account sourceAccount) {
                this.sourceAccount = sourceAccount;
                return this;
            }

            Builder withTargetAccount(Account targetAccount) {
                this.targetAccount = targetAccount;
                return this;
            }

            Builder withAmount(BigDecimal amount) {
                this.amount = amount;
                return this;
            }

            Builder withTransactionDateTime(Date transactionDateTime) {
                this.transactionDateTime = transactionDateTime;
                return this;
            }

            Builder withTransactionStatus(TransactionStatus transactionStatus) {
                this.transactionStatus = transactionStatus;
                return this;
            }

            Builder withTransactionDetailsForAccount(AccountId accountId) {
                this.transactionDetailsForAccount = accountId;
                return this;
            }

            TransactionDetails build() {
                Validate.notNull(sourceAccount);
                Validate.notNull(targetAccount);
                Validate.notNull(amount);

                User sourceAccountOwner = sourceAccount.getOwner();
                Participant sender = new Participant(sourceAccountOwner.getFirstName(), sourceAccountOwner.getLastName());
                AccountDetails sourceAccountDetails = new AccountDetails(sourceAccount.getNumber());

                User targetAccountOwner = targetAccount.getOwner();
                Participant receiver = new Participant(targetAccountOwner.getFirstName(), targetAccountOwner.getLastName());
                AccountDetails targetAccountDetails = new AccountDetails(targetAccount.getNumber());

                TransactionType transactionType = transactionDetailsForAccount.getValue().equals(sourceAccount.getId()) ? TransactionType.OUTGOING : TransactionType.INCOMING;

                return new TransactionDetails(sender, sourceAccountDetails, receiver, targetAccountDetails, amount, transactionDateTime, transactionStatus,
                                              transactionType);
            }
        }
    }

    static class Builder {
        private Account account;

        private Builder() {
        }

        Builder withAccount(Account account) {
            this.account = account;
            return this;
        }

        TransactionsHistory build() {
            Validate.notNull(account);

            Optional<Collection<Transaction>> accountOutgoingTransactions = Optional.ofNullable(account.getOutgoingTransaction());
            Optional<Collection<Transaction>> accountIncomingTransactions = Optional.ofNullable(account.getIncomingTransactions());

            List<TransactionDetails> transactions = Stream
                    .concat(accountOutgoingTransactions.orElseGet(List::of).stream(), accountIncomingTransactions.orElseGet(List::of).stream())
                    .map(this::buildTransactionDetails)
                    .sorted(Comparator.comparing(TransactionDetails::getTransactionDate))
                    .collect(Collectors.toList());

            return new TransactionsHistory(transactions);
        }

        private TransactionDetails buildTransactionDetails(Transaction transaction) {
            return TransactionDetails.builder()
                    .withSourceAccount(transaction.getSourceAccount())
                    .withTargetAccount(transaction.getTargetAccount())
                    .withAmount(transaction.getAmount())
                    .withTransactionDateTime(transaction.getTransactionDateTime())
                    .withTransactionStatus(transaction.getStatus())
                    .withTransactionDetailsForAccount(new AccountId(account.getId()))
                    .build();
        }
    }
}
