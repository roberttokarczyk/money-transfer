package test.backend.moneytransfer.account.service;

import test.backend.moneytransfer.domain.EntityId;

public final class AccountId extends EntityId {
    private final Long value;

    public AccountId(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}
