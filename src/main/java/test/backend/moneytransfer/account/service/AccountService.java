package test.backend.moneytransfer.account.service;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.account.dao.AccountDao;
import test.backend.moneytransfer.account.dao.AccountDaoImpl;
import test.backend.moneytransfer.account.domain.Account;

import java.sql.SQLException;

public class AccountService {

    private AccountDao accountDao;

    public AccountService() {
        try {
            accountDao = new AccountDaoImpl();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public TransactionsHistory getAccountHistory(AccountId accountId) throws SQLException {
        Validate.notNull(accountId);

        Account account = accountDao.queryForId(accountId.getValue());
        if (account == null) {
            return null;
        }

        return TransactionsHistory
                .builder()
                .withAccount(account)
                .build();
    }

    public Account getAccount(AccountId accountId) throws SQLException {
        Validate.notNull(accountId);
        Validate.isTrue(accountId.getValue() > 0);
        return accountDao.queryForId(accountId.getValue());
    }

    public void saveAccount(Account account) throws SQLException {
        Validate.notNull(account);

        accountDao.update(account);
    }
}
