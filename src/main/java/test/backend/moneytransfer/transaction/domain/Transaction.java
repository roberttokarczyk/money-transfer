package test.backend.moneytransfer.transaction.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.table.DatabaseTable;
import org.apache.commons.lang3.Validate;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.account.service.AccountService;
import test.backend.moneytransfer.domain.BaseEntity;
import test.backend.moneytransfer.exception.TransactionException;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

@DatabaseTable(tableName = "transaction")
public class Transaction extends BaseEntity {
    private AccountService accountService = new AccountService();

    @DatabaseField(generatedId = true, canBeNull = false)
    private Long id;

    @DatabaseField(columnName = "sourceAccountId", foreign = true, foreignAutoRefresh = true)
    private Account sourceAccount;

    @DatabaseField(columnName = "targetAccountId", foreign = true, foreignAutoRefresh = true)
    private Account targetAccount;

    @DatabaseField(columnName = "amount", canBeNull = false)
    private BigDecimal amount;

    @DatabaseField(columnName = "status", canBeNull = false, dataType = DataType.ENUM_STRING)
    private TransactionStatus status;

    @DatabaseField(columnName = "transactionDateTime", canBeNull = false, dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date transactionDateTime;

    private Transaction() {
        this.status = TransactionStatus.PENDING;
        this.transactionDateTime = new Date();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(Account targetAccount) {
        this.targetAccount = targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public static Transaction initializeTransaction(Account sourceAccount, Account targetAccount, BigDecimal amount) {
        Validate.notNull(sourceAccount);
        Validate.notNull(targetAccount);
        Validate.isTrue(amount.signum() > 0);

        Transaction transaction = new Transaction();
        transaction.setSourceAccount(sourceAccount);
        transaction.setTargetAccount(targetAccount);
        transaction.setAmount(amount);

        return transaction;
    }

    public Transaction perform() {
        this.status = TransactionStatus.IN_PROGRESS;
        Boolean transactionCompletedSuccessfully = false;
        try {
            transactionCompletedSuccessfully = TransactionManager
                    .callInTransaction(DataSourceConfiguration.getConnectionSource(), () -> {
                        boolean transactionSuccess;
                        boolean debitSuccess = sourceAccount.debit(amount);
                        if (debitSuccess) {
                            transactionSuccess = targetAccount.credit(amount);
                        } else {
                            throw new TransactionException("Debit from source account failed");
                        }

                        if (transactionSuccess) {
                            accountService.saveAccount(sourceAccount);
                            accountService.saveAccount(targetAccount);
                            return true;
                        } else {
                            throw new TransactionException("Credit for target account failed");
                        }
                    });
        } catch (SQLException ex) {
        }

        if (transactionCompletedSuccessfully != null && transactionCompletedSuccessfully) {
            this.status = TransactionStatus.SUCCESS;
        } else {
            this.status = TransactionStatus.FAILED;
        }

        return this;
    }
}
