package test.backend.moneytransfer.transaction.domain;

public enum TransactionStatus {
    PENDING,
    IN_PROGRESS,
    SUCCESS,
    FAILED
}
