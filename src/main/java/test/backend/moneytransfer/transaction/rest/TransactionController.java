package test.backend.moneytransfer.transaction.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import spark.ResponseTransformer;
import spark.Service;
import test.backend.moneytransfer.rest.JsonTransformer;
import test.backend.moneytransfer.rest.RestController;
import test.backend.moneytransfer.transaction.service.PerformTransactionParameters;
import test.backend.moneytransfer.transaction.service.TransactionDetails;
import test.backend.moneytransfer.transaction.service.TransactionId;
import test.backend.moneytransfer.transaction.service.TransactionService;
import test.backend.moneytransfer.util.IdValidator;

import javax.servlet.http.HttpServletResponse;

public class TransactionController implements RestController {
    private final ResponseTransformer responseTransformer = new JsonTransformer();
    private final TransactionService transactionService = TransactionService.getInstance();
    private final TransactionDetailsMapper transactionDetailsMapper = new TransactionDetailsMapper();
    private final IdValidator idValidator = new IdValidator();
    private final PerformTransactionRestParametersMapper performTransactionRestParametersMapper = new PerformTransactionRestParametersMapper();

    @Override
    public void configureEndpoints(Service spark, String path) {
        configureGetTransactionEndpoint(spark, path);
        configurePostTransactionEndpoint(spark, path);
    }

    private void configureGetTransactionEndpoint(Service spark, String path) {
        spark.get(path + "/transactions/:transactionId", "application/json", (request, response) -> {
            String transactionIdValue = request.params("transactionId");

            if (!transactionIdIsValid(transactionIdValue)) {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return "";
            }

            TransactionId transactionId = new TransactionId(Long.valueOf(transactionIdValue));
            TransactionDetails transactionDetails = transactionService.getTransactionDetails(transactionId);
            if(transactionDetails == null) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
                return "";
            } else {
                response.status(HttpServletResponse.SC_OK);
                return transactionDetailsMapper.toRestResponse(transactionDetails);
            }
        }, responseTransformer);
    }

    private void configurePostTransactionEndpoint(Service spark, String path) {
        spark.post(path + "/transactions", "application/json", (request, response) -> {
            String performTransactionRequestBody = request.body();

            if (performTransactionRequestBody == null || performTransactionRequestBody.isEmpty()) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
                return null;
            }

            PerformTransactionRestParameters performTransactionRestParameters = new ObjectMapper()
                    .readValue(performTransactionRequestBody, PerformTransactionRestParameters.class);

            if (!performTransactionParametersAreValid(performTransactionRestParameters)) {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return "";
            }

            PerformTransactionParameters performTransactionParameters = performTransactionRestParametersMapper
                    .toPerformTransactionParameters(performTransactionRestParameters);

            Long performedTransactionId = transactionService.performTransaction(performTransactionParameters);
            if (performedTransactionId != null) {
                response.status(HttpServletResponse.SC_OK);
                return new TransactionId(performedTransactionId);
            } else {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return "";
            }
        }, responseTransformer);
    }

    private boolean transactionIdIsValid(String transactionIdValue) {
        if (transactionIdValue == null || transactionIdValue.isEmpty()) {
            return false;
        }

        try {
            Long transactionId = Long.valueOf(transactionIdValue);
            return idValidator.isValid(transactionId);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean performTransactionParametersAreValid(PerformTransactionRestParameters performTransactionParameters) {
        return idValidator.isValid(performTransactionParameters.getSourceAccountId())
                && idValidator.isValid(performTransactionParameters.getTargetAccountId())
                && performTransactionParameters.getAmount().signum() > 0;
    }
}
