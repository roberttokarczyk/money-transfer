package test.backend.moneytransfer.transaction.rest;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.transaction.service.TransactionDetails;

class TransactionDetailsMapper {

    TransactionDetailsRestResponse toRestResponse(TransactionDetails transactionDetails) {
        Validate.notNull(transactionDetails);

        TransactionDetailsRestResponse.Participant sender = mapParticipant(transactionDetails.getSender());
        TransactionDetailsRestResponse.AccountDetails sourceAccountDetails = mapAccountDetails(transactionDetails.getSourceAccount());
        TransactionDetailsRestResponse.Participant receiver = mapParticipant(transactionDetails.getReceiver());
        TransactionDetailsRestResponse.AccountDetails targetAccountDetails = mapAccountDetails(transactionDetails.getTargetAccount());
        return new TransactionDetailsRestResponse(sender, sourceAccountDetails, receiver, targetAccountDetails, transactionDetails.getAmount(),
                                           transactionDetails.getTransactionDate(), transactionDetails.getTransactionStatus().name());
    }

    private TransactionDetailsRestResponse.Participant mapParticipant(TransactionDetails.Participant participant) {
        return new TransactionDetailsRestResponse.Participant(participant.getFirstName(), participant.getLastName());
    }

    private TransactionDetailsRestResponse.AccountDetails mapAccountDetails(TransactionDetails.AccountDetails accountDetails) {
        return new TransactionDetailsRestResponse.AccountDetails(accountDetails.getNumber());
    }
}
