package test.backend.moneytransfer.transaction.rest;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.transaction.service.PerformTransactionParameters;

class PerformTransactionRestParametersMapper {

    PerformTransactionParameters toPerformTransactionParameters(PerformTransactionRestParameters performTransactionRestParameters) {
        Validate.notNull(performTransactionRestParameters);

        return PerformTransactionParameters.builder()
                .withSourceAccountId(performTransactionRestParameters.getSourceAccountId())
                .withTargetAccountId(performTransactionRestParameters.getTargetAccountId())
                .withAmount(performTransactionRestParameters.getAmount())
                .build();
    }
}
