package test.backend.moneytransfer.transaction.service;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.backend.moneytransfer.account.concurrency.AccountLockByKey;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.account.service.AccountId;
import test.backend.moneytransfer.account.service.AccountService;
import test.backend.moneytransfer.concurrency.LockByKey;
import test.backend.moneytransfer.transaction.dao.TransactionDao;
import test.backend.moneytransfer.transaction.dao.TransactionDaoImpl;
import test.backend.moneytransfer.transaction.domain.Transaction;

import java.sql.SQLException;
import java.util.concurrent.locks.ReadWriteLock;

public class TransactionService {
    private final static Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    private final static TransactionService TRANSACTION_SERVICE = new TransactionService();
    private AccountService accountService;
    private LockByKey<Long> accountLockByKey;
    private TransactionDao transactionDao;

    private TransactionService() {
        this.accountService = new AccountService();
        this.accountLockByKey = new AccountLockByKey();
        try {
            transactionDao = new TransactionDaoImpl();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public static TransactionService getInstance() {
        return TRANSACTION_SERVICE;
    }

    public TransactionDetails getTransactionDetails(TransactionId transactionId) {
        Validate.notNull(transactionId);

        Transaction transaction = null;
        try {
            transaction = transactionDao.queryForId(transactionId.getValue());
        } catch (SQLException e) {
            LOGGER.error("Cannot find trasaction with id: {}", transactionId);
        }

        if (transaction == null) {
            return null;
        }

        return TransactionDetails.builder()
                .withSourceAccount(transaction.getSourceAccount())
                .withTargetAccount(transaction.getTargetAccount())
                .withAmount(transaction.getAmount())
                .withTransactionDateTime(transaction.getTransactionDateTime())
                .withTransactionStatus(transaction.getStatus())
                .build();
    }

    public Long performTransaction(PerformTransactionParameters performTransactionParameters) throws SQLException {
        Validate.notNull(performTransactionParameters);

        Long sourceAccountId = performTransactionParameters.getSourceAccount().getAccountId();
        ReadWriteLock sourceAccountLock = accountLockByKey.getLock(sourceAccountId);
        sourceAccountLock.writeLock().lock();
        Account sourceAccount = accountService.getAccount(new AccountId(sourceAccountId));
        if (sourceAccount == null) {
            sourceAccountLock.writeLock().unlock();
            return null;
        }
        Long targetAccountId = performTransactionParameters.getTargetAccount().getAccountId();
        ReadWriteLock targetAccountLock = accountLockByKey.getLock(targetAccountId);
        targetAccountLock.writeLock().lock();
        Account targetAccount = accountService.getAccount(new AccountId(targetAccountId));
        if (targetAccount == null) {
            sourceAccountLock.writeLock().unlock();
            targetAccountLock.writeLock().unlock();
            return null;
        }

        Transaction transaction = Transaction.initializeTransaction(sourceAccount, targetAccount, performTransactionParameters.getAmount());
        Transaction completedTransaction = transaction.perform();

        sourceAccountLock.writeLock().unlock();
        targetAccountLock.writeLock().unlock();

        try {
            transactionDao.create(completedTransaction);
            return completedTransaction.getId();
        } catch (SQLException e) {
            LOGGER.error("Cannot save transaction entity", e);
        }

        return null;
    }
}
