package test.backend.moneytransfer.transaction.service;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.domain.EntityId;

import java.util.Objects;

public final class TransactionId extends EntityId {
    private final Long value;

    public TransactionId(Long value) {
        Validate.notNull(value);
        Validate.isTrue(value > 0);

        this.value = value;
    }

    public Long getValue() {
        return value;
    }
}
