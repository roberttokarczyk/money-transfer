package test.backend.moneytransfer.transaction.service;

import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;

public class PerformTransactionParameters {
    private final Account sourceAccount;
    private final Account targetAccount;
    private final BigDecimal amount;

    private PerformTransactionParameters(Account sourceAccount, Account targetAccount, BigDecimal amount) {
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.amount = amount;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Account {
        private final Long accountId;

        private Account(Long accountId) {
            this.accountId = accountId;
        }

        public Long getAccountId() {
            return accountId;
        }
    }

    public static class Builder {
        private Long sourceAccountId;
        private Long targetAccountId;
        private BigDecimal amount;

        private Builder() {
        }

        public Builder withSourceAccountId(Long sourceAccountId) {
            this.sourceAccountId = sourceAccountId;
            return this;
        }

        public Builder withTargetAccountId(Long targetAccountId) {
            this.targetAccountId = targetAccountId;
            return this;
        }

        public Builder withAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public PerformTransactionParameters build() {
            Validate.notNull(sourceAccountId);
            Validate.notNull(targetAccountId);
            Validate.notNull(amount);

            Account sourceAccount = new Account(sourceAccountId);
            Account targetAccount = new Account(targetAccountId);
            return new PerformTransactionParameters(sourceAccount, targetAccount, amount);
        }
    }
}
