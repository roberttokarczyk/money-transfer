package test.backend.moneytransfer.transaction.service;

import org.apache.commons.lang3.Validate;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionDetails {

    private final Participant sender;
    private final AccountDetails sourceAccount;
    private final Participant receiver;
    private final AccountDetails targetAccount;
    private final BigDecimal amount;
    private final Date transactionDate;
    private final TransactionStatus transactionStatus;

    private TransactionDetails(Participant sender, AccountDetails sourceAccount, Participant receiver, AccountDetails targetAccount, BigDecimal amount,
                              Date transactionDate, TransactionStatus transactionStatus) {
        this.sender = sender;
        this.sourceAccount = sourceAccount;
        this.receiver = receiver;
        this.targetAccount = targetAccount;
        this.amount = amount;
        this.transactionDate = transactionDate;
        this.transactionStatus = transactionStatus;
    }

    public Participant getSender() {
        return sender;
    }

    public AccountDetails getSourceAccount() {
        return sourceAccount;
    }

    public Participant getReceiver() {
        return receiver;
    }

    public AccountDetails getTargetAccount() {
        return targetAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    static Builder builder() {
        return new Builder();
    }

    public static class Participant {
        private final String firstName;
        private final String lastName;

        private Participant(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }

    public static class AccountDetails {
        private final String number;

        private AccountDetails(String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }
    }

    static class Builder {
        private Account sourceAccount;
        private Account targetAccount;
        private BigDecimal amount;
        private Date transactionDateTime;
        private TransactionStatus transactionStatus;

        private Builder() {
        }

        Builder withSourceAccount(Account sourceAccount) {
            this.sourceAccount = sourceAccount;
            return this;
        }

        Builder withTargetAccount(Account targetAccount) {
            this.targetAccount = targetAccount;
            return this;
        }

        Builder withAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        Builder withTransactionDateTime(Date transactionDateTime) {
            this.transactionDateTime = transactionDateTime;
            return this;
        }

        Builder withTransactionStatus(TransactionStatus transactionStatus) {
            this.transactionStatus = transactionStatus;
            return this;
        }

        TransactionDetails build() {
            Validate.notNull(sourceAccount);
            Validate.notNull(targetAccount);
            Validate.notNull(amount);

            Participant sender = new Participant(sourceAccount.getOwner().getFirstName(), sourceAccount.getOwner().getLastName());
            AccountDetails sourceAccountDetails = new AccountDetails(sourceAccount.getNumber());

            Participant receiver = new Participant(targetAccount.getOwner().getFirstName(), targetAccount.getOwner().getLastName());
            AccountDetails targetAccountDetails = new AccountDetails(targetAccount.getNumber());

            return new TransactionDetails(sender, sourceAccountDetails, receiver, targetAccountDetails, amount, transactionDateTime, transactionStatus);
        }
    }
}
