package test.backend.moneytransfer.transaction.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.transaction.domain.Transaction;

import java.sql.SQLException;

public class TransactionDaoImpl extends BaseDaoImpl<Transaction, Long> implements TransactionDao {

    public TransactionDaoImpl() throws SQLException {
        super(DataSourceConfiguration.getConnectionSource(), Transaction.class);
    }
}
