package test.backend.moneytransfer.transaction.dao;

import com.j256.ormlite.dao.Dao;
import test.backend.moneytransfer.transaction.domain.Transaction;

public interface TransactionDao extends Dao<Transaction, Long> {
}
