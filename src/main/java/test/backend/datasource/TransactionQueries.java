package test.backend.datasource;

public class TransactionQueries {
    private static final String CREATE_TRANSACTION_TABLE_QUERY =  "CREATE TABLE Transaction ( " +
            " id INTEGER NOT NULL DEFAULT TRANSACTION_SEQ.nextval, " +
            " sourceAccountId INTEGER, " +
            " targetAccountId INTEGER, " +
            " amount DECIMAL(20, 2), " +
            " status VARCHAR(100), " +
            " transactionDateTime DATETIME DEFAULT CURRENT_TIMESTAMP(), " +
            " PRIMARY KEY (id), " +
            " FOREIGN KEY (sourceAccountId) references account(id)," +
            " FOREIGN KEY (targetAccountId) references account(id))";

    private final static String CREATE_TRANSACTION_SEQUENCE = "CREATE SEQUENCE TRANSACTION_SEQ";

    private final static String INSERT_TRANSACTIONS = "INSERT INTO TRANSACTION(sourceAccountId, targetAccountId, amount, status) VALUES" +
            " (1, 2, 10, 'SUCCESS')," +
            " (2, 1, 100, 'SUCCESS')," +
            " (1, 2, 1200, 'FAILED');";

    static String getCreateTransactionTableQuery() {
        return CREATE_TRANSACTION_TABLE_QUERY;
    }

    static String getCreateTransactionSequence() {
        return CREATE_TRANSACTION_SEQUENCE;
    }

    static String getInsertTransactions() {
        return INSERT_TRANSACTIONS;
    }
}
