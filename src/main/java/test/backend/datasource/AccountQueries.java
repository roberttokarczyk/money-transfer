package test.backend.datasource;

class AccountQueries {
    private static final String CREATE_ACCOUNT_TABLE_QUERY =  "CREATE TABLE Account ( " +
            " id INTEGER NOT NULL DEFAULT ACCOUNT_SEQ.nextval, " +
            " number VARCHAR(26), " +
            " balance DECIMAL(20, 2), " +
            " ownerId INTEGER, " +
            " currency VARCHAR(5), " +
            " inactive INTEGER DEFAULT 0, " +
            " PRIMARY KEY (id), " +
            " FOREIGN KEY (ownerId) references users(id))";

    private final static String CREATE_ACCOUNT_SEQUENCE = "CREATE SEQUENCE ACCOUNT_SEQ";

    private final static String INSERT_ACCOUNTS = "INSERT INTO ACCOUNT(number, balance, ownerId, currency) VALUES" +
            " ('22260000000050102222123456', 1000, 1, 'PLN')," +
            " ('22260000000050102222567890', 100, 2, 'PLN');";

    static String getCreateAccountTableQuery() {
        return CREATE_ACCOUNT_TABLE_QUERY;
    }

    static String getCreateAccountSequence() {
        return CREATE_ACCOUNT_SEQUENCE;
    }

    static String getInsertAccountsQuery() {
        return INSERT_ACCOUNTS;
    }
}
