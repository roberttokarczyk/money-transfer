package test.backend.datasource;

class UserQueries {
    private final static String CREATE_USERS_TABLE_QUERY =  "CREATE TABLE Users ( " +
            " id INTEGER NOT NULL DEFAULT USERS_SEQ.nextval, " +
            " firstName VARCHAR(50), " +
            " lastName VARCHAR(50), " +
            " PRIMARY KEY (id))";

    private final static String CREATE_USERS_SEQUENCE = "CREATE SEQUENCE USERS_SEQ";

    private final static String INSERT_USERS = "INSERT INTO USERS(firstName, lastName) VALUES" +
            " ('John', 'Doe')," +
            " ('Jane', 'Doe');";

    static String getCreateUsersTableQuery() {
        return CREATE_USERS_TABLE_QUERY;
    }

    static String getCreateUsersSequence() {
        return CREATE_USERS_SEQUENCE;
    }

    static String getInsertUsersQuery() {
        return INSERT_USERS;
    }
}
