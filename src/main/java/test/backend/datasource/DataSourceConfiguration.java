package test.backend.datasource;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataSourceConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceConfiguration.class);

    private final static String DATA_SOURCE_URL = "jdbc:h2:~/test";
    private final static String DATA_SOURCE_USER = "sa";
    private final static String DATA_SOURCE_PASSWORD = "";
    private static Connection CONNECTION = null;
    private static ConnectionSource CONNECTION_SOURCE = null;

    public DataSourceConfiguration() {
        CONNECTION = initDataSource();
        CONNECTION_SOURCE = initConnectionSource();
    }

    private Connection initDataSource() {
        try {
            Connection connection = DriverManager.getConnection(DATA_SOURCE_URL, DATA_SOURCE_USER, DATA_SOURCE_PASSWORD);
            LOGGER.info("Connection successfully initialized");
            initializeDataBase(connection);
            insertInitialData(connection);
            connection.setAutoCommit(true);
            return connection;
        } catch (SQLException e) {
            LOGGER.error("Cannot initialize data source", e);
            System.exit(0);
        }

        return null;
    }

    private ConnectionSource initConnectionSource() {
        try {
            ConnectionSource connectionSource = new JdbcConnectionSource(DATA_SOURCE_URL, DATA_SOURCE_USER, DATA_SOURCE_PASSWORD);
            LOGGER.info("Connection source successfully initialized");
            return connectionSource;
        } catch (SQLException e) {
            LOGGER.error("Cannot initialize connection source", e);
            System.exit(0);
        }

        return null;
    }

    private void initializeDataBase(Connection connection) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DROP ALL OBJECTS");
            statement.executeUpdate(UserQueries.getCreateUsersSequence());
            LOGGER.info("Users sequence successfully created");
            statement.executeUpdate(UserQueries.getCreateUsersTableQuery());
            LOGGER.info("Users table successfully created");
            statement.executeUpdate(AccountQueries.getCreateAccountSequence());
            LOGGER.info("Account sequence successfully created");
            statement.executeUpdate(AccountQueries.getCreateAccountTableQuery());
            LOGGER.info("Account table successfully created");
            statement.executeUpdate(TransactionQueries.getCreateTransactionSequence());
            LOGGER.info("Transaction sequence successfully created");
            statement.executeUpdate(TransactionQueries.getCreateTransactionTableQuery());
            LOGGER.info("Transaction table successfully created");
        } catch (SQLException e) {
            LOGGER.error("Cannot initialize database", e);
            System.exit(0);
        }
    }

    private void insertInitialData(Connection connection) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(UserQueries.getInsertUsersQuery());
            LOGGER.info("Users created");
            statement.executeUpdate(AccountQueries.getInsertAccountsQuery());
            LOGGER.info("Accounts created");
            statement.executeUpdate(TransactionQueries.getInsertTransactions());
            LOGGER.info("Transactions created");
        } catch (SQLException e) {
            LOGGER.error("Cannot insert initial data", e);
            System.exit(0);
        }
    }

    public static Connection getConnection() {
        return CONNECTION;
    }

    public static ConnectionSource getConnectionSource() {
        return CONNECTION_SOURCE;
    }
}
