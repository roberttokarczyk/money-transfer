package test.backend;

import spark.Service;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.account.rest.AccountController;
import test.backend.moneytransfer.rest.RestController;
import test.backend.moneytransfer.transaction.rest.TransactionController;

public class BankApplication {
    private final static int BANK_APPLICATION_API_PORT = 8080;
    private final static String BANK_APPLICATION_API_BASE_PATH = "/";
    public static void main( String[] args ) {
        new DataSourceConfiguration();
        Service spark = Service.ignite().port(BANK_APPLICATION_API_PORT);

        RestController transactionController = new TransactionController();
        transactionController.configureEndpoints(spark, BANK_APPLICATION_API_BASE_PATH);

        RestController accountController = new AccountController();
        accountController.configureEndpoints(spark, BANK_APPLICATION_API_BASE_PATH);
    }
}
