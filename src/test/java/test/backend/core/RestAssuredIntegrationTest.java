package test.backend.core;

import spark.Service;
import test.backend.datasource.DataSourceConfiguration;

public abstract class RestAssuredIntegrationTest {
    private final static DataSourceConfiguration DATA_SOURCE_CONFIGURATION = new DataSourceConfiguration();
    private final static int BANK_APPLICATION_API_PORT_FOR_TESTS = 9090;
    private final static Service SPARK = Service.ignite().port(BANK_APPLICATION_API_PORT_FOR_TESTS);

    public static int getPort() {
        return BANK_APPLICATION_API_PORT_FOR_TESTS;
    }

    public static Service getSpark() {
        return SPARK;
    }
}
