package test.backend.moneytransfer.account.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import test.backend.moneytransfer.account.dao.AccountDao;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.transaction.domain.Transaction;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;
import test.backend.moneytransfer.user.domain.User;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountDao accountDao;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void whenGetAccountHistory_givenAccountNotExist_thenReturnsNull() throws SQLException {
        // given
        when(accountDao.queryForId(eq(1L))).thenReturn(null);

        // when
        TransactionsHistory accountHistory = accountService.getAccountHistory(new AccountId(1L));

        // then
        assertThat(accountHistory).isNull();
    }

    @Test
    public void whenGetAccountHistory_givenAccountExists_andNoTransactionsWerePerformedOnAccount_thenReturnsEmptyTransactionHistory() throws SQLException {
        // given
        Account account = new Account();
        when(accountDao.queryForId(eq(1L))).thenReturn(account);

        // when
        TransactionsHistory accountHistory = accountService.getAccountHistory(new AccountId(1L));

        // then
        assertThat(accountHistory).isNotNull();
        assertThat(accountHistory.getTransactionsDetails()).isEmpty();
    }

    @Test
    public void whenGetAccountHistory_givenAccountExists_andOutgoingTransactionsWerePerformedOnAccount_thenReturnsTransactionHistoryWithTransactionsDetails() throws SQLException {
        // given
        User testAccountOwner = new User();
        testAccountOwner.setFirstName("John");
        testAccountOwner.setLastName("Doe");
        User otherAccountwner = new User();
        otherAccountwner.setFirstName("Jane");
        otherAccountwner.setLastName("Doe");
        Account testedAccount = new Account();
        testedAccount.setId(1L);
        testedAccount.setOwner(testAccountOwner);
        Account otherAccount = new Account();
        otherAccount.setId(2L);
        otherAccount.setOwner(otherAccountwner);
        Transaction transaction1 = Transaction.initializeTransaction(testedAccount, otherAccount, new BigDecimal("100.0"));
        transaction1.setStatus(TransactionStatus.SUCCESS);
        Transaction transaction2 = Transaction.initializeTransaction(testedAccount, otherAccount, new BigDecimal("1000.0"));
        transaction2.setStatus(TransactionStatus.FAILED);
        testedAccount.setOutgoingTransaction(List.of(transaction1, transaction2));
        when(accountDao.queryForId(eq(1L))).thenReturn(testedAccount);

        // when
        TransactionsHistory accountHistory = accountService.getAccountHistory(new AccountId(1L));

        // then
        assertThat(accountHistory).isNotNull();
        assertThat(accountHistory.getTransactionsDetails()).hasSize(2);
        assertThat(accountHistory.getTransactionsDetails()).extracting("sender.firstName", "sender.LastName").containsOnly(tuple("John", "Doe"));
        assertThat(accountHistory.getTransactionsDetails()).extracting("receiver.firstName", "receiver.LastName").containsOnly(tuple("Jane", "Doe"));
        assertThat(accountHistory.getTransactionsDetails()).extracting("amount", "transactionStatus", "transactionType")
                                                           .containsExactly(tuple(new BigDecimal("100.0"), TransactionStatus.SUCCESS, TransactionsHistory.TransactionDetails.TransactionType.OUTGOING),
                                                                            tuple(new BigDecimal("1000.0"), TransactionStatus.FAILED, TransactionsHistory.TransactionDetails.TransactionType.OUTGOING));
    }

    @Test
    public void whenGetAccountHistory_givenAccountExists_andOutgoingAndIcomingTransactionsWerePerformedOnAccount_thenReturnsTransactionHistoryWithTransactionsDetails() throws SQLException {
        // given
        User testAccountOwner = new User();
        testAccountOwner.setFirstName("John");
        testAccountOwner.setLastName("Doe");
        User otherAccountwner = new User();
        otherAccountwner.setFirstName("Jane");
        otherAccountwner.setLastName("Doe");
        Account testedAccount = new Account();
        testedAccount.setId(1L);
        testedAccount.setOwner(testAccountOwner);
        Account otherAccount = new Account();
        otherAccount.setId(2L);
        otherAccount.setOwner(otherAccountwner);
        Transaction transaction1 = Transaction.initializeTransaction(testedAccount, otherAccount, new BigDecimal("100.0"));
        transaction1.setStatus(TransactionStatus.SUCCESS);
        Transaction transaction2 = Transaction.initializeTransaction(otherAccount, testedAccount, new BigDecimal("1000.0"));
        transaction2.setStatus(TransactionStatus.FAILED);
        testedAccount.setOutgoingTransaction(List.of(transaction1, transaction2));
        when(accountDao.queryForId(eq(1L))).thenReturn(testedAccount);

        // when
        TransactionsHistory accountHistory = accountService.getAccountHistory(new AccountId(1L));

        // then
        assertThat(accountHistory).isNotNull();
        assertThat(accountHistory.getTransactionsDetails()).hasSize(2);
        assertThat(accountHistory.getTransactionsDetails()).extracting("sender.firstName", "sender.LastName")
                                                           .containsExactly(tuple("John", "Doe"),
                                                                            tuple("Jane", "Doe"));
        assertThat(accountHistory.getTransactionsDetails()).extracting("receiver.firstName", "receiver.LastName")
                                                           .containsExactly(tuple("Jane", "Doe"),
                                                                            tuple("John", "Doe"));
        assertThat(accountHistory.getTransactionsDetails()).extracting("amount", "transactionStatus", "transactionType")
                                                           .containsExactly(tuple(new BigDecimal("100.0"), TransactionStatus.SUCCESS, TransactionsHistory.TransactionDetails.TransactionType.OUTGOING),
                                                                            tuple(new BigDecimal("1000.0"), TransactionStatus.FAILED, TransactionsHistory.TransactionDetails.TransactionType.INCOMING));
    }

    @Test
    public void whenGetAccount_givenAccountNotExist_thenReturnsNull() throws SQLException {
        // given
        when(accountDao.queryForId(eq(1L))).thenReturn(null);

        // when
        Account account = accountService.getAccount(new AccountId(1L));

        // then
        assertThat(account).isNull();
    }

    @Test
    public void whenGetAccount_givenAccountExists_thenReturnsAccount() throws SQLException {
        // given
        Account account = new Account();
        when(accountDao.queryForId(eq(1L))).thenReturn(account);

        // when
        Account accountFromDB = accountService.getAccount(new AccountId(1L));

        // then
        assertThat(accountFromDB).isEqualTo(account);
    }

    @Test
    public void whenSaveAccount_givenAccountIsNotNull_thenUpdatesAccount() throws SQLException {
        // when
        Account account = new Account();
        accountService.saveAccount(account);

        // then
        verify(accountDao).update(eq(account));
    }
}