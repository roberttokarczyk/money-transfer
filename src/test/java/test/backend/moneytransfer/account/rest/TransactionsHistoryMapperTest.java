package test.backend.moneytransfer.account.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import test.backend.moneytransfer.account.service.TransactionsHistory;
import test.backend.moneytransfer.account.service.TransactionsHistory.TransactionDetails.AccountDetails;
import test.backend.moneytransfer.account.service.TransactionsHistory.TransactionDetails.Participant;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsHistoryMapperTest {
    private final TransactionsHistoryMapper transactionsHistoryMapper = new TransactionsHistoryMapper();

    @Test(expected = NullPointerException.class)
    public void whenToRestResponse_givenTransactionsHistoryIsNull_thenThrowsValidationException() {
        // when
        transactionsHistoryMapper.toRestResponse(null);
    }

    @Test
    public void whenToRestResponse_givenTransactionsHistoryIsNotNull_thenReturnsTransactionsHistoryRestResponse() {
        // given
        Participant sender = mock(Participant.class);
        when(sender.getFirstName()).thenReturn("John");
        when(sender.getLastName()).thenReturn("Doe");

        Participant receiver = mock(Participant.class);
        when(receiver.getFirstName()).thenReturn("Jane");
        when(receiver.getLastName()).thenReturn("Doe");

        AccountDetails sourceAccountDetails = mock(AccountDetails.class);
        when(sourceAccountDetails.getNumber()).thenReturn("123456789");

        AccountDetails targetAccountDetails = mock(AccountDetails.class);
        when(targetAccountDetails.getNumber()).thenReturn("987654321");

        TransactionsHistory.TransactionDetails transaction1Details = mock(TransactionsHistory.TransactionDetails.class);
        when(transaction1Details.getSender()).thenReturn(sender);
        when(transaction1Details.getReceiver()).thenReturn(receiver);
        Date transactionDate = new Date();
        when(transaction1Details.getTransactionDate()).thenReturn(transactionDate);
        when(transaction1Details.getAmount()).thenReturn(new BigDecimal("123.5"));
        when(transaction1Details.getTransactionStatus()).thenReturn(TransactionStatus.SUCCESS);
        when(transaction1Details.getTransactionType()).thenReturn(TransactionsHistory.TransactionDetails.TransactionType.OUTGOING);
        when(transaction1Details.getSourceAccount()).thenReturn(sourceAccountDetails);
        when(transaction1Details.getTargetAccount()).thenReturn(targetAccountDetails);
        TransactionsHistory transactionsHistory = mock(TransactionsHistory.class);
        when(transactionsHistory.getTransactionsDetails()).thenReturn(List.of(transaction1Details));

        // when
        TransactionsHistoryRestResponse transactionsHistoryRestResponse = transactionsHistoryMapper.toRestResponse(transactionsHistory);
        List<TransactionsHistoryRestResponse.TransactionDetails> transactionsDetails = transactionsHistoryRestResponse.getTransactionsDetails();

        assertThat(transactionsDetails).extracting("transactionType").containsExactly("OUTGOING");
        assertThat(transactionsDetails).extracting("transactionStatus").containsExactly("SUCCESS");
        assertThat(transactionsDetails).extracting("amount").containsExactly(new BigDecimal("123.5"));
        assertThat(transactionsDetails).extracting("sourceAccount.number").containsExactly("123456789");
        assertThat(transactionsDetails).extracting("targetAccount.number").containsExactly("987654321");
        assertThat(transactionsDetails).extracting("sender.firstName", "sender.lastName").containsExactly(tuple("John", "Doe"));
        assertThat(transactionsDetails).extracting("receiver.firstName", "receiver.lastName").containsExactly(tuple("Jane", "Doe"));
    }
}