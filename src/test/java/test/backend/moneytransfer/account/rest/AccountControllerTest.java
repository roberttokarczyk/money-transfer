package test.backend.moneytransfer.account.rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.*;
import test.backend.core.RestAssuredIntegrationTest;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.rest.RestController;

import java.util.HashMap;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

public class AccountControllerTest extends RestAssuredIntegrationTest {

    @BeforeClass
    public static void setUp() {
        RestController accountController = new AccountController();
        accountController.configureEndpoints(getSpark(), "/");
    }

    @AfterClass
    public static void tearDown() {
        getSpark().stop();
    }

    @Test
    public void whenGetAccountTransactionsHistory_givenAccountIdIsNotValid_thenReturnBadRequest() {
        given()
                .port(getPort())
                .pathParam("accountId", 0)
        .when()
                .get("/accounts/{accountId}/transactions")
        .then()
                .statusCode(400);
    }

    @Test
    public void whenGetAccountTransactionsHistory_givenAccountIdIsValid_andAccountDoesNotExist_thenReturnNotFound() {
        given()
                .port(getPort())
                .pathParam("accountId", 10)
        .when()
                .get("/accounts/{accountId}/transactions")
        .then()
                .statusCode(404);
    }

    @Test
    public void whenGetAccountTransactionsHistory_givenAccountIdIsValid_andAccountExists_thenReturnTransactionsHistory() {
        Response response = given()
                    .port(getPort())
                    .pathParam("accountId", 1)
            .when()
                    .get("/accounts/{accountId}/transactions")
            .then()
                    .statusCode(200)
                    .extract()
                    .response();

        JsonPath responseJson = response.body().jsonPath();
        List<HashMap<String, String>> transactionsDetails = responseJson.getList("transactionsDetails");
        assertThat(transactionsDetails).extracting("transactionType").containsExactlyInAnyOrder("OUTGOING", "OUTGOING", "INCOMING");
        assertThat(transactionsDetails).extracting("transactionStatus").containsExactlyInAnyOrder("SUCCESS", "SUCCESS", "FAILED");
        assertThat(transactionsDetails).extracting("amount").containsExactlyInAnyOrder(10.0f, 100.0f, 1200.0f);
        assertThat(transactionsDetails).extracting("sourceAccount").extracting("number")
                                      .containsExactlyInAnyOrder("22260000000050102222123456", "22260000000050102222123456", "22260000000050102222567890");
        assertThat(transactionsDetails).extracting("targetAccount").extracting("number")
                                      .containsExactlyInAnyOrder("22260000000050102222123456", "22260000000050102222567890", "22260000000050102222567890");
        assertThat(transactionsDetails).extracting("sender").extracting("firstName", "lastName").containsExactlyInAnyOrder(tuple("John", "Doe"),
                                                                                                                          tuple("John", "Doe"),
                                                                                                                          tuple("Jane", "Doe"));
        assertThat(transactionsDetails).extracting("receiver").extracting("firstName", "lastName").containsExactlyInAnyOrder(tuple("John", "Doe"),
                                                                                                                            tuple("Jane", "Doe"),
                                                                                                                            tuple("Jane", "Doe"));
    }
}