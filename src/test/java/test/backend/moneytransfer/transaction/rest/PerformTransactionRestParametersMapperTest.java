package test.backend.moneytransfer.transaction.rest;

import org.junit.Test;
import test.backend.moneytransfer.transaction.service.PerformTransactionParameters;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class PerformTransactionRestParametersMapperTest {
    private PerformTransactionRestParametersMapper performTransactionRestParametersMapper = new PerformTransactionRestParametersMapper();

    @Test(expected = NullPointerException.class)
    public void whenToPerformTransactionParameters_givenPerformTransactionRestParametersIsNull_thenThrowsValidationException() {
        // when
        performTransactionRestParametersMapper.toPerformTransactionParameters(null);
    }

    @Test
    public void whenToPerformTransactionParameters_givenPerformTransactionRestParametersIsNotNull_thenReturnsPerformTransactionParameters() {
        // given
        PerformTransactionRestParameters performTransactionRestParameters = new PerformTransactionRestParameters();
        performTransactionRestParameters.setAmount(new BigDecimal("1250.0"));
        performTransactionRestParameters.setSourceAccountId(1L);
        performTransactionRestParameters.setTargetAccountId(2L);

        // when
        PerformTransactionParameters performTransactionParameters = performTransactionRestParametersMapper
                .toPerformTransactionParameters(performTransactionRestParameters);

        // then
        assertThat(performTransactionParameters.getAmount()).isEqualTo("1250.0");
        assertThat(performTransactionParameters.getSourceAccount().getAccountId()).isEqualTo(1L);
        assertThat(performTransactionParameters.getTargetAccount().getAccountId()).isEqualTo(2L);
    }
}