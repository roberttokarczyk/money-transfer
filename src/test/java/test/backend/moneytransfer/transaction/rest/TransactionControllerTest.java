package test.backend.moneytransfer.transaction.rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.*;
import test.backend.core.RestAssuredIntegrationTest;
import test.backend.moneytransfer.rest.RestController;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionControllerTest extends RestAssuredIntegrationTest {

    @BeforeClass
    public static void setUp() {
        RestController transactionController = new TransactionController();
        transactionController.configureEndpoints(getSpark(), "/");
    }

    @AfterClass
    public static void tearDown() {
        getSpark().stop();
    }

    @Test
    public void whenGetTransaction_givenTransactionIdIsNotValid_thenReturnBadRequest() {
        given()
                .port(getPort())
                .pathParam("transactionId", 0)
        .when()
                .get("/transactions/{transactionId}")
        .then()
                .statusCode(400);
    }

    @Test
    public void whenGetTransaction_givenTransactionIdIsValid_andTransactionDoesNotExist_thenReturnNotFound() {
        given()
                .port(getPort())
                .pathParam("transactionId", 10)
                .when()
                .get("/transactions/{transactionId}")
                .then()
                .statusCode(404);
    }

    @Test
    public void whenGetTransaction_givenTransactionIdIsValid_andTransactionExists_thenReturnTransactionDetails() {
        Response response = given()
                .port(getPort())
                .pathParam("transactionId", 1)
        .when()
                .get("/transactions/{transactionId}")
        .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath responseJson = response.body().jsonPath();
        assertThat(responseJson.getString("transactionStatus")).isEqualTo("SUCCESS");
        assertThat(responseJson.getFloat("amount")).isEqualTo(10.0f);
        assertThat(responseJson.getString("sourceAccount.number")).isEqualTo("22260000000050102222123456");
        assertThat(responseJson.getString("targetAccount.number")).isEqualTo("22260000000050102222567890");
        assertThat(responseJson.getString("sender.firstName")).isEqualTo("John");
        assertThat(responseJson.getString("sender.lastName")).isEqualTo("Doe");
        assertThat(responseJson.getString("receiver.firstName")).isEqualTo("Jane");
        assertThat(responseJson.getString("receiver.lastName")).isEqualTo("Doe");
    }

    @Test
    public void whenPostTransaction_givenPerformTransactionRequestBodyIsEmpty_thenReturnNotFound() {
        given()
                .port(getPort())
                .contentType("application/json")
                .body("")
        .when()
                .post("/transactions")
        .then()
                .statusCode(404);
    }

    @Test
    public void whenPostTransaction_givenPerformTransactionRequestBodyIsNotValid_thenReturnBadRequest() {
        given()
                .port(getPort())
                .contentType("application/json")
                .body("{\"sourceAccountId\":0,\"targetAccountId\":2,\"amount\":100}")
        .when()
                .post("/transactions")
        .then()
                .statusCode(400);
    }

    @Test
    public void whenPostTransaction_givenPerformTransactionRequestBodyIsValid_andSourceAccountDoesNotExist_thenReturnBadRequest() {
        given()
                .port(getPort())
                .contentType("application/json")
                .body("{\"sourceAccountId\":3,\"targetAccountId\":2,\"amount\":100}")
        .when()
                .post("/transactions")
        .then()
                .statusCode(400);
    }

    @Test
    public void whenPostTransaction_givenPerformTransactionRequestBodyIsValid_andTargetAccountDoesNotExist_thenReturnBadRequest() {
        given()
                .port(getPort())
                .contentType("application/json")
                .body("{\"sourceAccountId\":1,\"targetAccountId\":3,\"amount\":100}")
        .when()
                .post("/transactions")
        .then()
                .statusCode(400);
    }

    @Test
    public void whenPostTransaction_givenPerformTransactionRequestBodyIsValid_thenPerformsTransaction_andReturnsOk() {
        Response response = given()
                .port(getPort())
                .contentType("application/json")
                .body("{\"sourceAccountId\":1,\"targetAccountId\":2,\"amount\":100}")
        .when()
                .post("/transactions")
        .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath responseJson = response.body().jsonPath();
        assertThat(responseJson.getLong("value")).isGreaterThan(3L);
    }
}