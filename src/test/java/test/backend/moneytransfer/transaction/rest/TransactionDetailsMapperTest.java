package test.backend.moneytransfer.transaction.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;
import test.backend.moneytransfer.transaction.service.TransactionDetails;
import test.backend.moneytransfer.transaction.service.TransactionDetails.AccountDetails;
import test.backend.moneytransfer.transaction.service.TransactionDetails.Participant;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionDetailsMapperTest {
    private TransactionDetailsMapper transactionDetailsMapper = new TransactionDetailsMapper();

    @Test(expected = NullPointerException.class)
    public void whenToRestResponse_givenTransactionDetailsIsNull_thenThrowsValidationException() {
        // when
        transactionDetailsMapper.toRestResponse(null);
    }

    @Test
    public void whenToRestResponse_givenTransactionDetailsIsNotNull_thenReturnsTransactionDetailsRestResponse() {
        // given
        Participant sender = mock(Participant.class);
        when(sender.getFirstName()).thenReturn("John");
        when(sender.getLastName()).thenReturn("Doe");

        Participant receiver = mock(Participant.class);
        when(receiver.getFirstName()).thenReturn("Jane");
        when(receiver.getLastName()).thenReturn("Doe");

        AccountDetails sourceAccountDetails = mock(AccountDetails.class);
        when(sourceAccountDetails.getNumber()).thenReturn("123456789");

        AccountDetails targetAccountDetails = mock(AccountDetails.class);
        when(targetAccountDetails.getNumber()).thenReturn("987654321");


        TransactionDetails transactionDetails = mock(TransactionDetails.class);
        when(transactionDetails.getAmount()).thenReturn(new BigDecimal("100.0"));
        Date transactionDate = new Date();
        when(transactionDetails.getTransactionDate()).thenReturn(transactionDate);
        when(transactionDetails.getTransactionStatus()).thenReturn(TransactionStatus.SUCCESS);
        when(transactionDetails.getSender()).thenReturn(sender);
        when(transactionDetails.getSourceAccount()).thenReturn(sourceAccountDetails);
        when(transactionDetails.getReceiver()).thenReturn(receiver);
        when(transactionDetails.getTargetAccount()).thenReturn(targetAccountDetails);

        // when
        TransactionDetailsRestResponse transactionDetailsRestResponse = transactionDetailsMapper.toRestResponse(transactionDetails);

        // then
        assertThat(transactionDetailsRestResponse.getTransactionStatus()).isEqualTo("SUCCESS");
        assertThat(transactionDetailsRestResponse.getAmount()).isEqualTo(new BigDecimal("100.0"));
        assertThat(transactionDetailsRestResponse.getSourceAccount().getNumber()).isEqualTo("123456789");
        assertThat(transactionDetailsRestResponse.getTargetAccount().getNumber()).isEqualTo("987654321");
        assertThat(transactionDetailsRestResponse.getSender().getFirstName()).isEqualTo("John");
        assertThat(transactionDetailsRestResponse.getSender().getLastName()).isEqualTo("Doe");
        assertThat(transactionDetailsRestResponse.getReceiver().getFirstName()).isEqualTo("Jane");
        assertThat(transactionDetailsRestResponse.getReceiver().getLastName()).isEqualTo("Doe");
    }
}