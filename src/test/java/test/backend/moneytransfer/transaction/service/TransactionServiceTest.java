package test.backend.moneytransfer.transaction.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import test.backend.datasource.DataSourceConfiguration;
import test.backend.moneytransfer.account.domain.Account;
import test.backend.moneytransfer.account.service.AccountId;
import test.backend.moneytransfer.account.service.AccountService;
import test.backend.moneytransfer.transaction.dao.TransactionDao;
import test.backend.moneytransfer.transaction.domain.Transaction;
import test.backend.moneytransfer.transaction.domain.TransactionStatus;
import test.backend.moneytransfer.user.domain.User;

import java.math.BigDecimal;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private TransactionDao transactionDao;

    @Mock
    private AccountService accountService;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    public void whenGetTransactionDetails_givenTransactionDoesNotExist_thenReturnsNull() {
        // when
        TransactionDetails transactionDetails = transactionService.getTransactionDetails(new TransactionId(1L));

        // then
        assertThat(transactionDetails).isNull();
    }

    @Test
    public void whenGetTransactionDetails_givenTransactionExists_thenReturnsTransactionDetails() throws SQLException {
        // given
        User testAccountOwner = new User();
        testAccountOwner.setFirstName("John");
        testAccountOwner.setLastName("Doe");
        User otherAccountOwner = new User();
        otherAccountOwner.setFirstName("Jane");
        otherAccountOwner.setLastName("Doe");
        Account testedAccount = new Account();
        testedAccount.setId(1L);
        testedAccount.setOwner(testAccountOwner);
        Account otherAccount = new Account();
        otherAccount.setId(2L);
        otherAccount.setOwner(otherAccountOwner);
        Transaction transaction = Transaction.initializeTransaction(testedAccount, otherAccount, new BigDecimal("100.0"));
        transaction.setStatus(TransactionStatus.SUCCESS);
        when(transactionDao.queryForId(eq(1L))).thenReturn(transaction);

        // when
        TransactionDetails transactionDetails = transactionService.getTransactionDetails(new TransactionId(1L));

        // then
        assertThat(transactionDetails).isNotNull();
        assertThat(transactionDetails).extracting("sender.firstName").isEqualTo("John");
        assertThat(transactionDetails).extracting("sender.LastName").isEqualTo("Doe");
        assertThat(transactionDetails).extracting("receiver.firstName").isEqualTo("Jane");
        assertThat(transactionDetails).extracting("receiver.LastName").isEqualTo("Doe");
        assertThat(transactionDetails).extracting("amount").isEqualTo(new BigDecimal("100.0"));
        assertThat(transactionDetails).extracting("transactionStatus").isEqualTo(TransactionStatus.SUCCESS);
    }

    @Test
    public void whenPerformTransaction_givenSourceAccountDoesNotExist_thenDoNothing() throws SQLException {
        // given
        Account targetAccount = new Account();
        when(accountService.getAccount(eq(new AccountId(1L)))).thenReturn(null);
        when(accountService.getAccount(eq(new AccountId(2L)))).thenReturn(targetAccount);
        PerformTransactionParameters performTransactionParameters = PerformTransactionParameters.builder()
                .withSourceAccountId(1L)
                .withTargetAccountId(2L)
                .withAmount(new BigDecimal("150"))
                .build();

        // when
        transactionService.performTransaction(performTransactionParameters);

        // then
        verify(transactionDao, never()).create(any(Transaction.class));
    }

    @Test
    public void whenPerformTransaction_givenTagetAccountDoesNotExist_thenDoNothing() throws SQLException {
        // given
        Account sourceAccount = new Account();
        when(accountService.getAccount(eq(new AccountId(1L)))).thenReturn(sourceAccount);
        when(accountService.getAccount(eq(new AccountId(2L)))).thenReturn(null);
        PerformTransactionParameters performTransactionParameters = PerformTransactionParameters.builder()
                                                                                                .withSourceAccountId(1L)
                                                                                                .withTargetAccountId(2L)
                                                                                                .withAmount(new BigDecimal("150"))
                                                                                                .build();

        // when
        transactionService.performTransaction(performTransactionParameters);

        // then
        verify(transactionDao, never()).create(any(Transaction.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPerformTransaction_givenTransactionAmountIsLessThanZero_thenThrowIllegalArgumentException() throws SQLException {
        // given
        Account sourceAccount = new Account();
        Account targetAccount = new Account();
        when(accountService.getAccount(eq(new AccountId(1L)))).thenReturn(sourceAccount);
        when(accountService.getAccount(eq(new AccountId(2L)))).thenReturn(targetAccount);
        PerformTransactionParameters performTransactionParameters = PerformTransactionParameters.builder()
                                                                                                .withSourceAccountId(1L)
                                                                                                .withTargetAccountId(2L)
                                                                                                .withAmount(new BigDecimal("-150"))
                                                                                                .build();

        // when
        transactionService.performTransaction(performTransactionParameters);
    }

    @Test
    public void whenPerformTransaction_givenTransactionAmountIsGreaterThanSourceAccountBalance_thenSaveTransactionWithStatusFailed_andDoNotModifyAccounts() throws SQLException {
        // given
        Account sourceAccount = new Account();
        sourceAccount.setId(1L);
        sourceAccount.setBalance(new BigDecimal("100.0"));
        Account targetAccount = new Account();
        targetAccount.setId(2L);
        targetAccount.setBalance(new BigDecimal("100.0"));
        when(accountService.getAccount(eq(new AccountId(1L)))).thenReturn(sourceAccount);
        when(accountService.getAccount(eq(new AccountId(2L)))).thenReturn(targetAccount);
        PerformTransactionParameters performTransactionParameters = PerformTransactionParameters
                .builder()
                .withSourceAccountId(1L)
                .withTargetAccountId(2L)
                .withAmount(new BigDecimal("150.0"))
                .build();

        // when
        new DataSourceConfiguration();
        transactionService.performTransaction(performTransactionParameters);

        // then
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionDao).create(transactionArgumentCaptor.capture());

        Transaction performedTransaction = transactionArgumentCaptor.getValue();

        assertThat(performedTransaction.getAmount()).isEqualTo(new BigDecimal("150.0"));
        assertThat(performedTransaction.getStatus()).isEqualTo(TransactionStatus.FAILED);
        assertThat(performedTransaction.getSourceAccount().getBalance()).isEqualTo(new BigDecimal("100.0"));
        assertThat(performedTransaction.getTargetAccount().getBalance()).isEqualTo(new BigDecimal("100.0"));
    }

    @Test
    public void whenPerformTransaction_givenTransactionAmountIsLessThanSourceAccountBalance_thenSaveTransactionWithStatusSuccess_andModifyAccounts() throws SQLException {
        // given
        Account sourceAccount = new Account();
        sourceAccount.setId(1L);
        sourceAccount.setBalance(new BigDecimal("100.0"));
        Account targetAccount = new Account();
        targetAccount.setId(2L);
        targetAccount.setBalance(new BigDecimal("100.0"));
        when(accountService.getAccount(eq(new AccountId(1L)))).thenReturn(sourceAccount);
        when(accountService.getAccount(eq(new AccountId(2L)))).thenReturn(targetAccount);
        PerformTransactionParameters performTransactionParameters = PerformTransactionParameters
                .builder()
                .withSourceAccountId(1L)
                .withTargetAccountId(2L)
                .withAmount(new BigDecimal("50.0"))
                .build();

        // when
        new DataSourceConfiguration();
        transactionService.performTransaction(performTransactionParameters);

        // then
        ArgumentCaptor<Transaction> transactionArgumentCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionDao).create(transactionArgumentCaptor.capture());

        Transaction performedTransaction = transactionArgumentCaptor.getValue();

        assertThat(performedTransaction.getAmount()).isEqualTo(new BigDecimal("50.0"));
        assertThat(performedTransaction.getStatus()).isEqualTo(TransactionStatus.SUCCESS);
        assertThat(performedTransaction.getSourceAccount().getBalance()).isEqualTo(new BigDecimal("50.0"));
        assertThat(performedTransaction.getTargetAccount().getBalance()).isEqualTo(new BigDecimal("150.0"));
    }
}